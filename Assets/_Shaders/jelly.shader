﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

Shader "Custom/jelly" {
	Properties {
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Tint ("Tint", Color) = (1,0.5,1,1)
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Alpha ("Transparency", Range(0,1)) = 0.75
	}
	SubShader {
		//Tags { "RenderType"="Opaque" }
		Tags { "Queue"="Overlay" "RenderType"="Transparent"  }
		LOD 200
		
		ZWrite Off
        Blend One OneMinusSrcAlpha

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 screenPos;
		};

		half _Glossiness;
		half _Metallic;
		float4 _Tint;
		float _Alpha;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void vert (inout appdata_full v, out Input o) {

			UNITY_INITIALIZE_OUTPUT(Input,o);
			v.vertex.x += sign(v.vertex.x) * sin(_Time.w) / 50;
			v.vertex.y += sign(v.vertex.y) * cos(_Time.w) / 40;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {

			IN.screenPos.x += sign(IN.screenPos.x) * sin(_Time.w)/50;
			IN.screenPos.y += sign(IN.screenPos.y) * cos(_Time.w)/50;

			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb + _Tint;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = _Alpha;
		}
		ENDCG
	}
	FallBack "Diffuse"
}