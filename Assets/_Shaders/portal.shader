﻿Shader "Custom/portal" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_AlphaTex ("Alpha Texture", 2D) = "white" {}
		_NoiseTex ("Noise", 2D) = "grey" {}
		_EmissionAmount ("Emission", Range(0,1)) = 0.5
		_NoiseAmount ("Noise Amount", Range(0,1)) = 0.75
		_Factor1 ("Noise Factor 1", float) = 0.25
		_Factor2 ("Noise Factor 2", float) = 0.5
	}
	SubShader {
		Tags {  "Queue"="Overlay" "RenderType"="Transparent" }
		LOD 200

		Zwrite off
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha:fade
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _AlphaTex;
		sampler2D _NoiseTex;

		struct Input {
			float2 uv_MainTex;
			float2 uv_AlphaTex;
		};

		float _EmissionAmount;
		float _NoiseAmount;
		float _Factor1;
		float _Factor2;
		fixed4 _Color;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		float2 noise2(float2 uv) {

			float noiseVal = tex2D(_NoiseTex, uv).r;
			float a = noiseVal + sin(_Time.y * _Factor1);
			float b = noiseVal + cos(_Time.y * _Factor2);

			return float2(a,b);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			//o.Emission = (tex2D (_AlphaTex, IN.uv_AlphaTex+noise2(IN.uv_AlphaTex)) + c).a * _EmissionAmount * _Color;
			o.Alpha = tex2D (_AlphaTex, IN.uv_AlphaTex).a * (1 + tex2D (_AlphaTex, IN.uv_AlphaTex+noise2(IN.uv_AlphaTex)).a * _NoiseAmount);
			o.Emission = c * o.Alpha * _Color * _EmissionAmount;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
