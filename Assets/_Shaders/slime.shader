﻿Shader "Custom/slime" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "white" {}
		_AlphaTex ("Shilouette", 2D) = "white" {}
	}
	SubShader {
		Tags { "Queue"="Overlay" "RenderType"="Transparent" }
		Tags { "ForceNoShadowCasting" = "True"}
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha:blend

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		sampler2D _AlphaTex;
		struct Input {
			float2 uv_MainTex;
		};
		fixed4 _Color;

		// #pragma instancing_options assumeuniformscaling
		//UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		//UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) ;
			fixed4 sil = tex2D(_AlphaTex, IN.uv_MainTex);
			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = 0.0;
			o.Smoothness = 1.0;
			o.Alpha = sil.a;
			o.Emission = _Color * 0.1;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
