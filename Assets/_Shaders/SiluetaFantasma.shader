﻿Shader "Custom/SiluetaFantasma" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Tint ("Emission Color", Color) = (1,1,0,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Emission ("Emission", Range(0,3)) = 0.5
		_MaxViewAngle ("Shilouette Size", Range(0,1)) = 0.5
		_NoiseTex ("Noise", 2D) = "grey" {}
		_NoiseAmount ("Noise Amount", Range(0,1)) = 0.5
	}
	SubShader {
		Tags { "Queue"="Overlay" "RenderType"="Transparent" }
		LOD 200
 		
		ZWrite Off
        //Blend SrcAlpha OneMinusSrcAlpha
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha:fade

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NoiseTex;

		struct Input {
			float2 uv_MainTex;
			float3 viewDir;
			float4 screenPos;
		};

		half _Emission;
		half _MaxViewAngle;
		half _NoiseAmount;
		fixed4 _Color;
		fixed4 _Tint;

		float rand(float2 co){
    		float aux = frac(sin(dot(co.xy ,float2(12.9898,78.233))) * 43758.5453 );
			return sin(dot(co.x, co.y) * _Time.w + 0.1); /*+ cos(dot(co.x, co.y)*/;
		}

		float2 noise2(float2 uv) {

			float noiseVal = tex2D(_NoiseTex, uv).r;
			float a = noiseVal + sin(_Time.y * 0.25);
			float b = noiseVal + cos(_Time.y * 0.5);

			return float2(a,b);
		}

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {

			float3 N = o.Normal;
			float viewAngle = dot(IN.viewDir, N);

			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;

			//IN.uv_MainTex.x *= _CosTime.w;
			//IN.uv_MainTex.y *= _SinTime.w;

			o.Metallic = 1.0;
			o.Smoothness = 1.0;
			//float aux = clamp(dot(noise2(IN.uv_MainTex).x, noise2(IN.uv_MainTex).y) * _NoiseAmount, 0.1, _MaxViewAngle);
			float aux = clamp(1+ dot(noise2(IN.uv_MainTex).x, noise2(IN.uv_MainTex).y) * _NoiseAmount, 0.0, 1.0);
			if (viewAngle < _MaxViewAngle*aux) {
				o.Alpha = 1.0 /*- (viewAngle*1.25)*/;
			} else {
				o.Alpha = (1.0-viewAngle);
			}
			//o.Alpha *= clamp(1+ dot(noise2(IN.uv_MainTex).x, noise2(IN.uv_MainTex).y) * _NoiseAmount, 0.0, 1.0);
			o.Emission = o.Alpha;
			o.Emission += _NoiseAmount * clamp(dot(noise2(IN.uv_MainTex).x, noise2(IN.uv_MainTex).y) * _NoiseAmount, 0.0, 1.0);
			o.Emission *= _Tint * _Emission;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
