﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowWave : MonoBehaviour
{

    [SerializeField] float lifeTime = 5f;
    [SerializeField] float endValue = 25f;

    float initValue = 0f;
    bool animDelayEnd = false;
    Vector3 vEndValue = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        vEndValue = new Vector3(endValue, 1, endValue);
        transform.localScale = new Vector3(initValue, 1, initValue);
        StartCoroutine(AnimDelay());
    }

    // Update is called once per frame
    void Update()
    {
        if (animDelayEnd)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, vEndValue, 0.1f);

            if ((transform.localScale - vEndValue).magnitude < 0.5f)
                Destroy(gameObject, 0.01f);
        }
    }

    IEnumerator AnimDelay()
    {
        yield return new WaitForSeconds(0.5f);
        animDelayEnd = true;
    }
}
