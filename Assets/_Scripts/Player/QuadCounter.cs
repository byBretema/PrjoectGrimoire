﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadCounter : MonoBehaviour
{
    #region Publics
    public bool isFree;
    public static QuadCounter instance;
    #endregion

    #region Mono Methods
    // Use this for initialization
    void Start()
    {
        isFree = true;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            isFree = false;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Enemy")
        {
            isFree = true;
        }
    }
    #endregion

    #region Other Methods

    #endregion
}


