﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ViewMenu : MonoBehaviour,IPointerClickHandler, IPointerEnterHandler,IPointerExitHandler  {

	public Color color;
	private  static Text text;

	void Start () {

	}

	/// <summary>
	/// 	Raises the pointer click event.
	/// </summary>
	/// <param name="evenData">Even data.</param>
	public void OnPointerClick(PointerEventData evenData){
		Debug.LogWarning ("Entro en clok" + gameObject.name);
		//ControllerMenu.GetInstance().AccionBoton(gameObject.name);
		ControllerMenu.GetInstance().AccionBoton(gameObject.name);

	}
	public void Click(){
		Debug.LogWarning ("Entro en clok" + gameObject.name);
		//ControllerMenu.GetInstance().AccionBoton(gameObject.name);
		ControllerMenu.GetInstance().AccionBoton(gameObject.name);

	}

	/// <summary>
	/// Raises the pointer enter event.
	/// </summary>
	/// <param name="evenData">Even data.</param>
	public void OnPointerEnter(PointerEventData evenData){
		string s = evenData.pointerEnter.name;
		if(string.Compare(s,"TextC")==0){
			GameObject.Find("Boton Nueva Partida").transform.GetChild(0).gameObject.GetComponent<Image>().enabled= false;
			GameObject.Find("Boton Nueva Partida").transform.GetChild(1).gameObject.GetComponent<Text>().color = Color.white; 
		}
		Debug.LogWarning ("valor evendata button: " + evenData.pointerEnter);
		//transform.scaleTo (0.1f, new Vector3 (1.3f, 1.3f, 1.3f));
		gameObject.GetComponent<Image>().enabled= true;
		gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().color= Color.cyan; 
	}

	/// <summary>
	/// Raises the pointer exit event.
	/// </summary>
	/// <param name="evenData">Even data.</param>
	public void OnPointerExit(PointerEventData evenData){
		//transform.scaleTo (0.1f, new Vector3 (1.0f,1.0f,1.0f));
		gameObject.GetComponent<Image>().enabled= false;		
		gameObject.transform.GetChild(0).gameObject.GetComponent<Text>().color = Color.white; 
	}
}