﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewUI : MonoBehaviour {

	// Use this for initialization
	[SerializeField]
	private Text textEnemys;

	[SerializeField] Text h;
	[SerializeField] Text min;
	[SerializeField] Text sec;

	public int totalEnemys=0;


	int ho =0;
	int mins=0;
	int seg=0;
	float nextTime=0.5f;

	private static ViewUI ui_view;


	public static ViewUI instance
	{
		get
		{
			if (!ui_view)
			{
				ui_view = FindObjectOfType(typeof(ViewUI)) as ViewUI;

				if (!ui_view)
				{
					Debug.LogError("There needs to be one active EventManager script on a GameObject in your scene.");
				}

			}

			return ui_view;
		}
	}


	public void AddEnemy(){

		textEnemys.text = (int.Parse (textEnemys.text)+1).ToString();
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Clock ();
		UpdateNumEnemy ();
	}



	#region Functions

	void Clock(){
		nextTime = Time.time;
		ho = (int)nextTime % 1;
		mins = (int)nextTime / 60;
		seg = (int)nextTime % 60;
		h.text=ho.ToString();
		min.text = ":"+mins.ToString ();
		sec.text = ":"+seg.ToString ();
	}

	void UpdateNumEnemy(){
		textEnemys.text = totalEnemys.ToString ();
	
	
	}

	#endregion
}
