﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    [SerializeField] private float time = 6.0f;
    void Start() { Destroy(gameObject, time); }
}
