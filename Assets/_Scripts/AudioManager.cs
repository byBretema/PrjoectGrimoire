﻿// ------------------------ //
//  "Dont Paint Me" - 2017  //
//    Daniel Camba Lamas    //
//  <cambalamas@gmail.com>  //
// ------------------------ //

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public bool loop;
    public string name;
    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume = 1f;

    [Range(-3f, 3f)]
    public float pitch = 1f;

    [HideInInspector]
    public AudioSource source;
}

public class AudioManager : MonoBehaviour
{

    // ************************************************************************
    //	--- VARIABLES ---
    // ************************************************************************

    public List<Sound> sounds;
    public static AudioManager instance;

    // ************************************************************************
    //	--- UNITY BASE METHODS ---
    // ************************************************************************

    void Awake()
    {
        // Singleton
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        // Create audio sources
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    void Start()
    {
        instance.Play("bg_music");
    }

    // ************************************************************************
    //	--- (Public) HELPERS ---
    // ************************************************************************

    void SoundAction(string name, string action, float value = 1.0f)
    {
        var s = sounds.Find(sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound '" + name + "' not found!");
            return;
        }
        switch (action)
        {
            case "Play": s.source.Play(); break;
            case "Stop": s.source.Stop(); break;
            case "Pitch": s.source.pitch = value; break;
            default: Debug.LogWarning("Action not recognized!"); break;
        }
    }

    // Excute PLAY method of source var of sound.
    public void Play(string name)
    {
        SoundAction(name, "Play");
    }

    // Excute STOP method of source var of sound.
    public void Stop(string name)
    {
        SoundAction(name, "Stop");
    }

    // Excute PITCH method of source var of sound.
    public void Pitch(string name, float value)
    {
        SoundAction(name, "Pitch", value);
    }
}
