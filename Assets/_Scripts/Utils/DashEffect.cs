﻿using UnityEngine;
using System.Collections;

public class DashEffect : MonoBehaviour {
	public SkinnedMeshRenderer skin;
	[SerializeField]
	private Color colorLerp1;
	[SerializeField]
	private Color colorLerp2;

	Mesh baked;
	ParticleSystem particle;
	ParticleSystemRenderer render;
	public bool emit;
	public float coolDown = 0.5f;
	public Color lerpedColor;
	float interval = 0;

	// Use this for initialization
	void Start () {

		if(!skin)
			this.enabled = false;

		particle = GetComponent<ParticleSystem>();
		render = GetComponent<ParticleSystemRenderer>();

		lerpedColor = particle.main.startColor.color;
	}

	// Update is called once per frame
	void Update () {
		//emit[true]
		if(emit){
			
			interval -= Time.deltaTime;
	
			if(interval < 0){
	
				GameObject newEmitter = Instantiate(gameObject, transform.position, transform.rotation) as GameObject;

				newEmitter.GetComponent<DashEffect>().EmitMesh();
	
				interval = coolDown;
			}

		}else{

			interval = coolDown;
		}
	}


	public void EmitMesh () {
		
		emit = false;
		baked = new Mesh();
		skin.BakeMesh(baked);
		particle = GetComponent<ParticleSystem>();
		Color c = Color.Lerp(colorLerp1, colorLerp2, Mathf.PingPong(Time.time, 1));
		particle.startColor = c;
		render = GetComponent<ParticleSystemRenderer>();
		render.mesh = baked;
		particle.Play();
		Destroy(gameObject, particle.main.duration);
	}
}