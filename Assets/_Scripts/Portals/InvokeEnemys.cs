﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeEnemys : MonoBehaviour
{
    // Use this for initialization
    [Header("GameObjects")]
    [SerializeField]
    private GameObject enemy; // enemigo a invocar
    [SerializeField]
    private Transform player; // transform del player

    [Header("Parametres")]
    [SerializeField]
	public  int time = 4; //cada cuento se invoca un enemigo
    [SerializeField]
	public int numEnemy = 10; //cuantos enemigos se invocan en total

	public int count; // count enemy create

    private const string PATH_PREFABS = "Prefabs/";

    void Start()
    {
		Debug.Log ("Estoy en el sart");
        InvokeRepeating("CreateEnemy", 2, time);
    }

    // Update is called once per frame
    void Update()
    {
		if (count == numEnemy || PortalController.instance.desactivate==false)
        {
            CancelInvoke("CreateEnemy");
        }

		 
	
    }

	public void newInvoke (){
	
		InvokeRepeating("CreateEnemy", 2, time);
	
	}

    /// <summary>
    /// Creates the enemy.
    /// </summary>
    private void CreateEnemy()
    {
        Debug.LogWarning("---Path:" + PATH_PREFABS);

        //GameObject go;
        //go = GameObject.Instantiate(Resources.Load<GameObject>(PATH_PREFABS + "Enemy"));
        //go.name = "Enemy";
        //go.transform.position = transform.position;

        //go.transform.SetParent(null);
        //go.GetComponent<EnemyNav>().target = player;


        Vector3 portalPos = transform.position;
        Vector3 floorPos = GameObject.FindWithTag("Floor").transform.position;
        Vector3 instPos = new Vector3(portalPos.x, floorPos.y, portalPos.z);

        GameObject go2;
        go2 = GameObject.Instantiate(enemy, instPos, Quaternion.identity);
        //go2.GetComponent<Enemy>().target = player;
        go2.name = enemy.name;


        count++;
        UltimateController.instance.SaveEnemy(go2);
        Debug.LogWarning("----------- CREATING ENEMY ----------------");
    }
}
