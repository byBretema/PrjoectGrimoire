﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalController : MonoBehaviour {

	// Use this for initialization
	[SerializeField] GameObject portalPlane;
	public int numCristals=2;
	public bool desactivate = true;
	public Transform transformplane;
	[SerializeField] AudioSource closePortal;
	[SerializeField] AudioSource openPortal;

	private static PortalController portalController;
	public static PortalController instance
	{
		get
		{
			if (!portalController)
			{
				portalController = FindObjectOfType(typeof(PortalController)) as PortalController;
				if (!portalController)
					Debug.LogError("There needs to be one active EventManager script on a GameObject in your scene.");
			}
			return portalController;
		}
	}

	void Start () {
		transformplane= portalPlane.transform; // para guardar los valores de escalado del plano del portal 
	}

	// Update is called once per frame
	void Update () {
		Debug.LogWarning("ACTIVATE PORTAL " +transformplane.localScale );


		if (numCristals == 0 && desactivate == true) { // desactivams el portal
			this.desactivate= false;
			this.closePortal.Play ();
			this.portalPlane.transform.scaleTo (2.0f, Vector3.zero);	
			this.portalPlane.GetComponent<InvokeEnemys> ().enabled = false; // dejamos de emitir enemigos
			GameManager.instance.numPortals++;
		}

		 

	}

	public void ActivatePortal (){
		Debug.LogWarning("ACTIVATE PORTAL 2222 " +transformplane.localScale );
		this.desactivate= true;
		this.portalPlane.GetComponent<InvokeEnemys> ().newInvoke ();// dejamos de emitir enemigos

		portalPlane.transform.scaleTo (2.0f, Vector3.one);	

	}

	public void reactivarCristales(){
		
	}
}


