﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{

    [Header("Parameters")]
    [SerializeField] float speed = 2f;
    [SerializeField] float lifeTime = 3.5f;
    [SerializeField] float hitLifeTime;

    [Header("Backend")]
    bool stop = false;
    bool animDelayEnd = false;

    // Use this for initialization
    void Start()
    {
        hitLifeTime = lifeTime * 0.2f;
        transform.position += transform.forward * 0.5f;
        transform.localScale = Vector3.zero;
        StartCoroutine(AnimDelay());
        Destroy(gameObject, lifeTime);
    }

    void Update()
    {
        //if (!stop)
        //{
        var tgt = GameObject.FindWithTag("ThrowableHitPoint").transform.position;
        //tgt = new Vector3(tgt.x, transform.position.y, tgt.z);
        transform.LookAt(tgt);
        //transform.rotation = new Quaternion(0f, transform.rotation.y,
        //                                    0f, transform.rotation.w);
        transform.position = Vector3.Lerp(transform.position, tgt, speed * Time.deltaTime);
        transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, speed * Time.deltaTime);
    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            StartCoroutine(Stop());
            Destroy(gameObject, hitLifeTime);
        }
    }

    IEnumerator HitAnimation()
    {
        Render(false, false, true, false);
        yield return null;
    }

    IEnumerator Stop()
    {
        StartCoroutine(HitAnimation());
        yield return new WaitForSeconds(lifeTime * 0.5f);
        stop = true;
    }

    IEnumerator AnimDelay()
    {
        Render(false, false, false, false);
        yield return new WaitForSeconds(0.05f);
        Render(true, true, false, true);
        animDelayEnd = true;
    }

    void Render(bool halo, bool trail, bool particles, bool mesh)
    {
        (GetComponent("Halo") as Behaviour).enabled = halo;
        transform.GetChild(0).gameObject.SetActive(trail);
        transform.GetChild(1).gameObject.SetActive(particles);
        gameObject.GetComponent<MeshRenderer>().enabled = mesh;
    }
}
