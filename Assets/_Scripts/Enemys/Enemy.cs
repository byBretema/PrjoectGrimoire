﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public enum AttackTypes { Distance, Melee }

    #region Exposed Vars
    [SerializeField] GameObject throwable;
    [SerializeField] AttackTypes AttackType;
    [SerializeField] int m_hurt;
	[SerializeField] GameObject effectDead;
    public bool isAttacking { get; set; }
    public int hurt { get { return m_hurt; } set { m_hurt = value; } }
	public int Totallife;
    private int life;
    [SerializeField] float coolDownTime = 1f;
	private float frecAttack;
	private float fReach;
	private float precison;
    #endregion

	#region Factors
	private float fHealth = 0.6941f;
	private float fDamage= 0.8235f;
	public bool isBoss=false;

	public float dIndex = 0;
	#endregion

    #region Private Vars
    static string SWORD = "Sword";
    // Managers
    NavMeshAgent meshAgent;
    Animator animController;
    // Areas
    GameObject distCollider;
    GameObject meleeCollider;
    Vector3 fixedWayPoint;
    // Behaviors
    bool coolDown = false;
    float throwPauseTime = 2f;
	[SerializeField] bool ThrowableIsAlive = false;
    // bool isAttacking = false;
	GameObject efect;
    #endregion

    #region Const Vars
    const float wpMargin = 5f;
    const float distMargin = 16f;
    const float meleeMargin = 9f;
    const float wanderMargin = 55f;
    #endregion

    #region MonoBehaviour Methods
    void Start()
    {

        isAttacking = false;
        // Managers
        meshAgent = GetComponent<NavMeshAgent>();
        animController = gameObject.GetComponentInChildren<Animator>();
        Wander(); // <--- This assign a random "fixedWayPoint" on the mesh.

		frecAttack = 1 / Totallife;
		if (AttackTypes.Melee == AttackType) {
			fReach = 1.0f;
			precison = 1.0f;
		} else {
			fReach = 2.0f;
			precison = 0.75f;
		}

		dIndex = CalculateIndex ();
    }

    void Update()
    {
        if (meshAgent.isOnNavMesh)
        {
            meshAgent.destination = fixedWayPoint;

            if (NearToPlayer()) { Attack(); }
            else
            {
                if (PosReached() || isAttacking)
                {
                    Wander();

                    if (isAttacking)
                    {
                        isAttacking = false;
                        animController.SetBool("attack", false);
                        EnemyManager.instance.DecAttackers(AttackType);
                    }
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(SWORD) && PlayerController.instance.attack)
        {
            //////////////////////////////////////////////////
            //////////////// MANAGE DAMAGE !
            //////////////////////////////////////////////////
            PlayerController.instance.NumHits++;
			Debug.LogWarning("muere enemigo");

			this.Totallife--;
			if (this.Totallife == 0)
			{
			 //Enemy dead
				ViewUI.instance.totalEnemys++;
                EnemyManager.instance.DecAttackers(AttackType);

				 efect = GameObject.Instantiate(effectDead, this.transform.position, effectDead.transform.rotation);
				StartCoroutine ("deadEnemy");
				//gameObject.SetActive (false);

				//Destroy(gameObject, 0.01f);
			}
        }
		if (isBoss == true && other.CompareTag ("Floor")) {
			triggerShockwave.instance.startShockwave ();
		}
    }
    #endregion

    #region Other Methods

	float CalculateIndex(){
		return ((Totallife * fHealth) + (m_hurt * frecAttack * precison)) * fReach;
	}



    bool NearToPlayer()
    {
        Vector3 playerPos = PlayerController.instance.transform.position;
        float margin = (AttackType == AttackTypes.Melee) ? meleeMargin : distMargin;
        return (transform.position - playerPos).magnitude < margin - wpMargin;
    }
    bool PosReached()
    {
        return (transform.position - fixedWayPoint).magnitude < wpMargin;
    }

    void Walk()
    {
        animController.Play("Walk");
        meshAgent.speed = 5f;
        meshAgent.acceleration = 15f;
    }

    void Stop()
    {
        meshAgent.enabled = false;
        meshAgent.speed = 0f;
        meshAgent.acceleration = 0f;
        meshAgent.enabled = true;
    }

    void Attack()
    {
        if (coolDown) return;

        Stop();
        if (!isAttacking) { EnemyManager.instance.IncAttackers(AttackType); }
        isAttacking = true;
        transform.LookAt(PlayerController.instance.transform.position);
        transform.rotation = new Quaternion(0f, transform.rotation.y,
                                            0f, transform.rotation.w);



        if (AttackType == AttackTypes.Distance && !ThrowableIsAlive)
        {
            float animTime = animController.GetCurrentAnimatorStateInfo(0).normalizedTime;
            bool animAttack = animController.GetCurrentAnimatorStateInfo(0).IsName("Attack");

            float margin_animTime = 0.01f;
            if (this.transform.name.Contains("UC_Pingu")) { margin_animTime = 0.2f; }


            bool throwAttack = (Mathf.Abs(animTime - 0.5f) < margin_animTime && animAttack);
           // Debug.Log("animtime data = " + (Mathf.Abs(animTime - 0.5f)));
            //Debug.Log("anim is attack = " + animAttack);
            if (throwAttack) { ThrowableIsAlive = true; StartCoroutine(ThrowAttackObj()); }
            animController.SetBool("attack", true);
        }

        if (AttackType == AttackTypes.Melee)
        {
            animController.Play("Attack");
        }
    }

    void Wander()
    {
        Walk();
        float wanderDist = (AttackType == AttackTypes.Melee) ? meleeMargin : distMargin;
        if (EnemyManager.instance.IsAbusive(AttackType)) { wanderDist = wanderMargin; }

        NavMeshHit hit;
        Vector3 playerPos = PlayerController.instance.transform.position;
        Vector3 newPos = playerPos + (wanderDist * Random.insideUnitSphere);
        NavMesh.SamplePosition(newPos, out hit, wanderDist, 1);
        fixedWayPoint = hit.position;
    }

    IEnumerator CoolDown()
    {
        coolDown = true;
        bool playingAttack = !(
            animController.GetCurrentAnimatorStateInfo(0).normalizedTime < 1f
            && animController.GetCurrentAnimatorStateInfo(0).IsName("Attack"));
        yield return new WaitUntil(() => { return playingAttack == false; });
        //animController.SetBool("attack", false);
        yield return new WaitForSeconds(coolDownTime);
        coolDown = false;
    }

    // Throw the paint sphere
    IEnumerator ThrowAttackObj()
    {
        if (throwable != null)
        {
            var instPos = transform.GetChild(1).transform.position;
            var go = Instantiate(throwable, instPos, Quaternion.Euler(0, 0, 0), null);
			Debug.Log("El nmbre es " +this.name);
			go.tag = this.name;
            //yield return new WaitForSeconds(0.1f);
            ThrowableIsAlive = false;
        }
        else
        {
            Debug.LogError("Distance enemy without throwable object");
        }
        yield return null;
    }

	IEnumerator deadEnemy(){
		yield return new WaitForSeconds(2.0f);
		Destroy (efect, 0.1f);


		Destroy (gameObject);


	}
    #endregion
}
